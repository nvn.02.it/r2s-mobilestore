package com.category;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.mobilestore.product.dtos.ProductDTO;
import com.mobilestore.product.entities.Product;
import com.mobilestore.product.repositories.ProductRepository;
import com.mobilestore.product.services.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ModelMapper modelMapper;

    @Test
    public void testGetAllProducts_EmptyPage() {
        // Mock the repository to return an empty page
        Pageable pageable = PageRequest.of(0, 10);
        List<Product> emptyList = new ArrayList<>();
        Page<Product> emptyPage = new PageImpl<>(emptyList, pageable, 0);
        when(productRepository.findAll(pageable)).thenReturn(emptyPage);

        // Call the service method
        Page<ProductDTO> allProducts = productService.getAllProduct(1);

        // Assert that the returned page is empty
        assertTrue(allProducts.isEmpty());
        assertEquals(0, allProducts.getTotalElements());
        assertEquals(0, allProducts.getTotalPages());
    }

    @Test
    public void testGetAllProducts_WithProducts() {
        Pageable pageable = PageRequest.of(0, 10);

        List<Product> products = new ArrayList<>();
        products.add(new Product(1L, "Phone", "phone.jpg", 500.0, "A great phone", 10));
        products.add(new Product(2L, "Laptop", "laptop.jpg", 1000.0, "A powerful laptop", 5));
        Page<Product> productPage = new PageImpl<>(products, pageable, products.size());
        when(productRepository.findAll(pageable)).thenReturn(productPage);

        ProductDTO productDTO1 = new ProductDTO(1L, "Phone", "phone.jpg", 500.0, "A great phone", 10);
        ProductDTO productDTO2 = new ProductDTO(2L, "Laptop", "laptop.jpg", 1000.0, "A powerful laptop", 5);
        when(modelMapper.map(products.get(0), ProductDTO.class)).thenReturn(productDTO1);
        when(modelMapper.map(products.get(1), ProductDTO.class)).thenReturn(productDTO2);

        // Call the service method
        Page<ProductDTO> allProducts = productService.getAllProduct(1);

        // Assert that the returned page contains the correct products
        assertEquals(2, allProducts.getTotalElements());
        assertEquals(1, allProducts.getTotalPages());
        assertEquals(productDTO1, allProducts.getContent().get(0));
        assertEquals(productDTO2, allProducts.getContent().get(1));
    }
}
