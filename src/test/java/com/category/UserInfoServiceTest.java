package com.category;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Optional;

import com.mobilestore.user.dtos.UserDTO;
import com.mobilestore.user.entities.Role;
import com.mobilestore.user.entities.UserInfo;
import com.mobilestore.user.repositories.UserInfoRepository;
import com.mobilestore.user.services.UserInfoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserInfoServiceTest {

    @Mock
    private UserInfoRepository repository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserInfoService userInfoService;

    String username = "nonexistinguser";

    UserInfo userInfo = new UserInfo();

    Role role = new Role(1,"ROLE_USER");

    UserDTO userDto = new UserDTO();



    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        userInfo.setName(username);
        userInfo.setEmail("test@example.com");
        userInfo.setPassword("hashedPassword");
        userInfo.setRole(role);

        userDto.setName("testuser");
        userDto.setEmail("test@example.com");
        userDto.setPassword("password");

    }

    @Test
    public void testLoadUserByUsername_ExistingUser() {

        when(repository.findByName(username)).thenReturn(Optional.of(userInfo));

        UserDetails userDetails = userInfoService.loadUserByUsername(username);

        assertNotNull(userDetails);
        assertEquals(username, userDetails.getUsername());
        assertEquals(userInfo.getPassword(), userDetails.getPassword());
    }

    @Test
    public void testLoadUserByUsername_NonExistingUser() {

        when(repository.findByName(username)).thenReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class, () -> userInfoService.loadUserByUsername(username));
    }

    @Test
    public void testSaveUser() {

        UserInfo userToSave = new UserInfo();
        userToSave.setName(userDto.getName());
        userToSave.setEmail(userDto.getEmail());
        userToSave.setPassword("hashedPassword");

        when(passwordEncoder.encode(userDto.getPassword())).thenReturn("hashedPassword");

        userInfoService.saveUser(userDto);

        verify(repository, times(1)).save(userToSave);
    }
}
