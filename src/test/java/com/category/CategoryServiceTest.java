package com.category;

import com.mobilestore.category.entities.Category;
import com.mobilestore.category.repositories.CategoryRepository;
import com.mobilestore.category.services.CategoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


import java.util.ArrayList;
import java.util.List;

public class CategoryServiceTest {

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private MessageSource messageSource;

    @InjectMocks
    private CategoryService categoryService;

    List<Category> categories = new ArrayList<>();

    Category category = new Category(1, "Category 1");

    Integer categoryId = 2;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        categories.add(new Category(1, "Category 1"));
        categories.add(new Category(2, "Category 2"));
        categories.add(new Category(1, "Category 3"));
        categories.add(new Category(2, "Category 4"));
        categories.add(new Category(1, "Category 5"));
        categories.add(new Category(2, "Category 6"));

    }

    @Test
    public void testFindAll() {

        Pageable pageable = PageRequest.of(0, 2);
        Mockito.when(categoryRepository.findAll(pageable)).thenReturn(new PageImpl<>(categories.subList(0, 2)
                , pageable, categories.size()));

        Page<Category> result = categoryService.findAll(1);

        Assertions.assertEquals(2, result.getContent().size());
        Assertions.assertEquals(categories.get(0), result.getContent().get(0));
        Assertions.assertEquals(categories.get(1), result.getContent().get(1));
    }

    @Test
    public void testFindByName() {

        Pageable pageable = PageRequest.of(0, 2);
        Mockito.when(categoryRepository.findByNameContaining("Category", pageable))
                .thenReturn(new PageImpl<>(categories.subList(0, 2), pageable, categories.size()));

        Page<Category> result = categoryService.findByName("Category", 1);

        Assertions.assertEquals(2, result.getContent().size());
        Assertions.assertEquals(categories.get(0), result.getContent().get(0));
        Assertions.assertEquals(categories.get(1), result.getContent().get(1));
    }

    @Test
    public void testFindById() {

        Mockito.when(categoryRepository.getById(1)).thenReturn(category);

        Category result = categoryService.findById(1);

        Assertions.assertEquals(category, result);
    }


    @Test
    public void testSaveCategory() {

        Mockito.when(categoryRepository.save(category)).thenReturn(category);

        Category result = categoryService.saveCategory(category);

        Assertions.assertEquals(category, result);
    }

    @Test
    public void deleteCategoryById_categoryExists() {

        Mockito.when(categoryRepository.existsById(categoryId)).thenReturn(true);

        categoryService.deleteCategoryById(categoryId);

        Mockito.verify(categoryRepository).deleteById(categoryId);

    }

    @Test
    public void deleteCategoryById_categoryNotExists() {

        Mockito.when(categoryRepository.existsById(categoryId)).thenReturn(false);
        Mockito.when(messageSource.getMessage("error.delete.message", null, null))
                .thenReturn("Error message");

        try {
            categoryService.deleteCategoryById(categoryId);
        } catch (IllegalArgumentException e) {
            Mockito.verify(categoryRepository,  Mockito.never()).deleteById(categoryId);
            Assertions.assertEquals("Error message", e.getMessage());
        }
    }

}
