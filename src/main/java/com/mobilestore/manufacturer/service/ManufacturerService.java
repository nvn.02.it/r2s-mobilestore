package com.mobilestore.manufacturer.service;

import com.mobilestore.manufacturer.entities.Manufacturer;
import com.mobilestore.manufacturer.repositories.ManufacturerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ManufacturerService {
    private final ManufacturerRepository manufacturerRepository;

    /**
     * Retrieves all manufacturers from the repository.
     *
     * @return a list of all {@link Manufacturer} entities.
     */
    public List<Manufacturer> findAll() {
        return manufacturerRepository.findAll();
    }
}
