package com.mobilestore.manufacturer.repositories;

import com.mobilestore.manufacturer.entities.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {
    /**
     * Finds the first manufacturer by the given ID.
     *
     * @param id The ID of the manufacturer to find.
     * @return The manufacturer with the given ID, or null if no manufacturer found.
     */
    public Manufacturer findFirstById(Long id);
}
