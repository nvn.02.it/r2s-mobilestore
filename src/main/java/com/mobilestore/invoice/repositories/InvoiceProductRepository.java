package com.mobilestore.invoice.repositories;

import com.mobilestore.invoice.entities.InvoiceProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface InvoiceProductRepository extends JpaRepository<InvoiceProduct,Integer> {
}
