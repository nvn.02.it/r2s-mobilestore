package com.mobilestore.invoice.entities;

import jakarta.persistence.*;
import lombok.*;


import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany(mappedBy = "invoice" ,  cascade = CascadeType.ALL ,fetch = FetchType.EAGER)
    private List<InvoiceProduct>  invoiceProducts;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime dateCreate;

    private Double total;

    public Invoice() {
        this.dateCreate = LocalDateTime.now();
    }
}
