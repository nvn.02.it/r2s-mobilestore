package com.mobilestore.invoice.services;

import com.mobilestore.cart.dtos.CartDTO;
import com.mobilestore.invoice.entities.Invoice;
import com.mobilestore.invoice.entities.InvoiceProduct;
import com.mobilestore.invoice.repositories.InvoiceRepository;
import com.mobilestore.product.entities.Product;
import com.mobilestore.product.repositories.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class InvoiceService {

    private final InvoiceRepository invoiceRepository;
    private final ProductRepository productRepository;

    /**
     * Saves the invoice and updates product quantities in stock.
     *
     * @param cartDTOList   The list of cart items.
     * @param productMap    The mapping from product ID to product.
     */
    public void saveInvoice(List<CartDTO> cartDTOList, Map<Long, Product> productMap) {
        Invoice invoice = new Invoice();

        List<InvoiceProduct> invoiceProductList = new ArrayList<>();

        double invoiceTotal = 0;
        for (CartDTO cartDTO : cartDTOList) {
            Product product = productMap.get(cartDTO.getId());

            int remainingQuantity = product.getQuantityInStock() - cartDTO.getQuantity();
            product.setQuantityInStock(remainingQuantity);

            double invoiceProductTotal = cartDTO.getQuantity() * product.getPrice();
            invoiceTotal += invoiceProductTotal;

            InvoiceProduct invoiceDetail = new InvoiceProduct(
                    null,
                    product,
                    invoice,
                    cartDTO.getQuantity(),
                    invoiceProductTotal);
            invoiceProductList.add(invoiceDetail);
        }

        invoice.setInvoiceProducts(invoiceProductList);
        invoice.setTotal(invoiceTotal);

        invoiceRepository.save(invoice);

        // Update product quantities in stock
        productRepository.saveAll(productMap.values());
    }

}
