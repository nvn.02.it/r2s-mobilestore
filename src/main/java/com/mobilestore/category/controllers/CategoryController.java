package com.mobilestore.category.controllers;

import com.mobilestore.category.entities.Category;
import com.mobilestore.category.services.CategoryService;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/category")
@AllArgsConstructor
public class CategoryController {

    private final MessageSource messageSource;

    private final  CategoryService categoryService;

    /**
     * Retrieve a page of categories and render the list-category view.
     *
     * @param model The Model object to add attributes for rendering the view.
     * @param page  The page number of categories to retrieve. Default value is 1.
     * @return The name of the view to render.
     */
    @GetMapping("/")
    public String getPageCategory(Model model
            , @RequestParam(value = "page", defaultValue = ("1")) @Min(1) Integer page) {
        Page<Category> categories = categoryService.findAll(page);
        model.addAttribute("list_category", categories);
        return "list-category";
    }

    @GetMapping("/admin")
    public String getPageAdmin(Model model
            , @RequestParam(value = "page", defaultValue = ("1")) @Min(1) Integer page) {
        Page<Category> categories = categoryService.findAll(page);
        model.addAttribute("list_category", categories);
        return "category-admin";
    }


    /**
     * Retrieve categories by name containing a specific keyword and render the list-category view.
     *
     * @param name  The keyword to search for in category names.
     * @param model The Model object to add attributes for rendering the view.
     * @param page  The page number of categories to retrieve. Default value is 1.
     * @return The name of the view to render.
     */
    @GetMapping("/search")
    public String findNameOrId(@RequestParam("name") String name, Model model
            , @RequestParam(value = "page", defaultValue = ("1")) @Min(1) Integer page) {
        model.addAttribute("name", name);
        Page<Category> categories = categoryService.findByName(name, page);
        model.addAttribute("list_category", categories);
        return "list-category";
    }

    /**
     * Add new category with the specific name and redirect to the category page.
     *
     * @param name               The name of the new category to be added.
     * @param redirectAttributes The RedirectAttributes object used to add flash attributes.
     * @return A redirection to the category page.
     */
    @PostMapping("/add")
    public String addCategory(@RequestParam("name") String name, RedirectAttributes redirectAttributes) {
        try {
            Category category = new Category(null, name);
            categoryService.saveCategory(category);
            redirectAttributes.addFlashAttribute("result",
                    messageSource.getMessage("success.message", null, null));
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/category/admin";
    }


    /**
     * Update an existing category and redirect to the category page.
     *
     * @param category           The updated category object.
     * @param redirectAttributes The RedirectAttributes object used to add flash attributes.
     * @return A redirection to the category page.
     */
    @PostMapping("/edit")
    public String editCategory(@ModelAttribute("category") Category category, RedirectAttributes redirectAttributes) {
        try {
            categoryService.saveCategory(category);
            redirectAttributes.addFlashAttribute("result",
                    messageSource.getMessage("success.message", null, null));
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/category/admin";
    }

    /**
     * Delete a category by its ID and redirect to the category page.
     *
     * @param id                 The ID of the category to delete.
     * @param redirectAttributes The RedirectAttributes object used to add flash attributes.
     * @return A redirection to the category page.
     */
    @GetMapping("/delete/{id}")
    public String deleteCategory(@PathVariable("id") @NotNull Integer id, RedirectAttributes redirectAttributes) {
        try {
            categoryService.deleteCategoryById(id);
            redirectAttributes.addFlashAttribute("result",
                    messageSource.getMessage("success.message", null, null));
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/category/admin";
    }


}
