package com.mobilestore.category.controllers;

import com.mobilestore.category.entities.Category;
import com.mobilestore.category.services.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/category/api")
@AllArgsConstructor
public class CategoryRestController {

    private final CategoryService categoryService;

    @GetMapping("/getbyid/{id}")
    public ResponseEntity<Map<String, Object>> getCategoryById(@PathVariable("id") Integer id) {
        Category category = categoryService.findById(id);

        Map<String, Object> response = new HashMap<>();
        response.put("id", category.getId());
        response.put("name", category.getName());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
