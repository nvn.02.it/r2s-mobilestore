package com.mobilestore.category.services;

import com.mobilestore.category.entities.Category;
import com.mobilestore.category.repositories.CategoryRepository;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CategoryService {


    private static final int DEFAULT_PAGE_SIZE = 10;

    private static final int MAX_NAME_LENGTH = 50;

    private final CategoryRepository categoryRepository;

    private final MessageSource messageSource;

    /**
     * find all category in a specific page.
     *
     * @param page The page number to fetch categories from. page number start from 1
     * @return A page of categories containing paginated result
     */
    public Page<Category> findAll(Integer page){
        Pageable pageable = PageRequest.of(page-1,DEFAULT_PAGE_SIZE);
        return categoryRepository.findAll(pageable);
    }

    /**
     * find all category by their names containing a specific keyword , in a specific page.
     *
     * @param name The keyword to search for in category name.
     * @param page The page number to fetch categories from. page number start from 1.
     * @return  A page of categories whose name contain the specific keyword, containing paginated results.
     */
    public Page<Category> findByName(String name,Integer page) {
        Pageable pageable = PageRequest.of(page-1,DEFAULT_PAGE_SIZE);
        return categoryRepository.findByNameContaining(name,pageable);
    }

    /**
     * find a category by its ID.
     *
     * @param id The id of the category to find.
     * @return The category with specific ID.
     */
    public Category findById(Integer id){
        return categoryRepository.getById(id);
    }

    /**
     * Save a category to the database
     *
     * @param category The category to be saved.
     * @return The saved category.
     * @throws IllegalArgumentException if the category name is empty,exceeds the maximum length or already exists.
     */
    public Category saveCategory(Category category) {
        String name = category.getName();

        if (name.isEmpty() || name.length() > MAX_NAME_LENGTH) {
            throw new IllegalArgumentException(
                    messageSource.getMessage("validation.error",null,null));
        }

        if (categoryRepository.existsByName(name)) {
            throw new IllegalArgumentException("Danh mục đã tồn tại");
        }

        return categoryRepository.save(category);
    }

    /**
     * Delete a category by its ID.
     *
     * @param id The ID of the category to delete
     * @throws IllegalArgumentException if no category with the specified ID exists
     */
    public void deleteCategoryById(int id) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(
                        messageSource.getMessage("error.delete.message", null, null)));
        categoryRepository.delete(category);
    }

    /**
     * Find all categories without pagination.
     *
     * @return A list of all categories.
     */
    public List<Category> findAllNotPageable(){
        return categoryRepository.findAll();
    }
}
