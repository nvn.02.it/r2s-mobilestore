package com.mobilestore.category.repositories;

import com.mobilestore.category.entities.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {

    /**
     * Check if a category with given name exits.
     *
     * @param name The name of the category to check for existence.
     * @return True if a category with the given name exits, otherwise false.
     */
    public  boolean existsByName(String name);

    /**
     * find all category whose names contain the keyword
     *
     * @param name      The keyword to search for in category names
     * @param pageable  Pagination information for the query result
     * @return          A page containing categories whose names contain the keyword
     */
    public  Page<Category> findByNameContaining(String name, Pageable pageable);

    /**
     * Finds the first category by the given ID.
     *
     * @param id The ID of the category to find.
     * @return The category with the given ID, or null if no category found.
     */
    public Category findFirstById(Integer id);
}
