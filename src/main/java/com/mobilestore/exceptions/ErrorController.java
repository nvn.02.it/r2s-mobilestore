package com.mobilestore.exceptions;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {

    @GetMapping("/error-access-denied")
    public String handleAccessDenied() {
        return "error-access-denied";
    }
}