package com.mobilestore.jwt.handlers;

import com.mobilestore.jwt.services.JwtService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * JwtAuthenticationSuccessHandler is responsible for handling successful authentication events.
 * Upon successful authentication, it generates a JWT token for the authenticated user and saves it in a cookie.
 * It then redirects the user to the category page.
 */
@Component
@AllArgsConstructor
public class JwtAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final  JwtService jwtService;

    /**
     * Invoked when authentication is successful.
     * Generates a JWT token for the authenticated user, saves it in a cookie, and redirects the user to the category page.
     * @param request The HttpServletRequest representing the request.
     * @param response The HttpServletResponse representing the response.
     * @param authentication The Authentication object representing the authenticated user.
     * @throws IOException If an I/O error occurs while handling the request.
     * @throws ServletException If a servlet exception occurs.
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {

        String username = authentication.getName();

        String role = authentication.getAuthorities().iterator().next().getAuthority();
        HttpSession session = request.getSession();
        session.setAttribute("username", username);
        session.setAttribute("role", role);

        String token = jwtService.generateToken(username);

        Cookie cookie = new Cookie("token", token);
        cookie.setMaxAge(Integer.MAX_VALUE);
        cookie.setPath("/");
        response.addCookie(cookie);

        response.sendRedirect("/");
    }
}
