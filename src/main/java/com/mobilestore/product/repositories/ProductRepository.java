package com.mobilestore.product.repositories;

import com.mobilestore.product.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {
    /**
     * Finds the first product by the given ID.
     *
     * @param id The ID of the product to find.
     * @return The product with the given ID, or null if no product found.
     */
    public Product findFirstById(Long id);
}
