package com.mobilestore.product.controllers;

import com.mobilestore.product.dtos.ProductDTO;
import com.mobilestore.product.services.ProductService;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class ProductRestController {

    private  final ProductService productService;

    @GetMapping("/load-more-product")
    public Page<ProductDTO> listAllProduct(@RequestParam(value = "page", defaultValue = ("1")) @Min(1) Integer page
            , Model model){
        return productService.getAllProduct(page);
    }
}
