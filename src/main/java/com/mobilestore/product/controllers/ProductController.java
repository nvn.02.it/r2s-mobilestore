package com.mobilestore.product.controllers;

import com.mobilestore.category.services.CategoryService;
import com.mobilestore.manufacturer.service.ManufacturerService;
import com.mobilestore.product.dtos.CUProductDTO;
import com.mobilestore.product.dtos.ProductDTO;
import com.mobilestore.product.services.ProductService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.jsoup.Jsoup;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

@Controller
@RequestMapping("/product")
@AllArgsConstructor
public class ProductController {

    private final ProductService productService;

    private final String LIST_PRODUCT_VIEW = "list-product";
    private final String PRODUCT_VIEW_DETAIL = "detail-product";
    private final String PRODUCT_CU_VIEW = "cu-product";


    private final ManufacturerService manufacturerService;

    private final CategoryService categoryService;

    private final static Logger logger = Logger.getLogger(ProductController.class);

    private final MessageSource messageSource;

    private String getMessage(String code) {
        return messageSource.getMessage(code, null, LocaleContextHolder.getLocale());
    }

    /**
     * Populates the model attributes required for rendering the Add new/Update (CU) product form.
     *
     * @param model       The model to add attributes for the view
     * @param cUProductDTO The CUProductDTO object representing product information
     * @param id          The ID of the product (null for new product)
     */
    public void createInfoCUPage(Model model,CUProductDTO cUProductDTO, Long id)
    {
        String subHeading = getMessage("product.create-form-sub-heading");
        String formTitle = getMessage("product.create-form-title");
        String url = "/product/add";
        String buttonSubmit = getMessage("button.submit.product.create.name");

        if (id != null) {
            subHeading = getMessage("product.edit-form-sub-heading");
            formTitle = getMessage("product.edit-form-title");
            url = "/product/edit/"+ id;
            buttonSubmit = getMessage("button.submit.product.edit.name");
        }

        model.addAttribute("subHeading", subHeading);
        model.addAttribute("formTitle", formTitle);
        model.addAttribute("url", url);
        model.addAttribute("buttonSubmit", buttonSubmit);
        model.addAttribute("product", cUProductDTO);
        model.addAttribute("listManufacturer", manufacturerService.findAll());
        model.addAttribute("listCategory", categoryService.findAllNotPageable());
    }

    /**
     * Maps a ProductDTO object to a CUProductDTO object.
     *
     * @param productDTO The ProductDTO object to map
     * @return The mapped CUProductDTO object
     */
    private CUProductDTO mapToCUProductDTO(ProductDTO productDTO) {
        return CUProductDTO.builder()
                .id(productDTO.getId())
                .name(productDTO.getName())
                .price(productDTO.getPrice())
                .quantityInStock(productDTO.getQuantityInStock())
                .description(productDTO.getDescription())
                .manufacturer(productDTO.getManufacturer().getId())
                .category(productDTO.getCategory().getId())
                .productCondition(productDTO.getProductCondition())
                .existingImages(productDTO.getImages())
                .build();
    }

    /**
     * Controller method to list all products.
     *
     * @param page  The page number for pagination (default is 1)
     * @param model The model to add attributes for the view
     * @return The view name for displaying the list of products
     */
    @GetMapping("/")
    public String listAllProduct(@RequestParam(value = "page", defaultValue = ("1")) @Min(1) Integer page
            , Model model) {
        Page<ProductDTO> productDTOS = productService.getAllProduct(page);
        productDTOS.forEach(productDTO -> {
            String parsedDescription = Jsoup.parse(productDTO.getDescription()).text();
            productDTO.setDescription(parsedDescription);
        });
        model.addAttribute("listProduct",productDTOS );
        return LIST_PRODUCT_VIEW;
    }

    /**
     * Controller method to handle retrieval of the add product page.
     *
     * @param model The model to add attributes for the view
     * @return The view name for adding a new product
     */
    @GetMapping("/add")
    public String getAddProductPage(Model model) {
        createInfoCUPage(model, CUProductDTO.builder().build(), null);
        return PRODUCT_CU_VIEW;
    }

    /**
     * Controller method to handle retrieval of the edit product page by ID.
     *
     * @param id The ID of the product to edit
     * @param model The model to add attributes for the view
     * @return The view name for editing the product
     */
    @GetMapping("/edit/{id}")
    public String getEditProductPage(@PathVariable("id") @NotNull Long id, Model model) {
        try {
            ProductDTO productDTO = productService.convertToDTO(productService.getProductById(id));

            createInfoCUPage(model, mapToCUProductDTO(productDTO), id);
        } catch (Exception e) {
            logger.error(e);
            return "page-not-found";
        }

        return PRODUCT_CU_VIEW;
    }

    /**
     * Controller method to handle saving a new product or updating an existing product.
     *
     * @param cUProductDTO The DTO containing product information to save or update
     * @param result The binding result for validation errors
     * @param model The model to add attributes for the view
     * @param files The multipart files representing product images
     * @param id The ID of the product to update (null if adding new product)
     * @return A redirection based on the operation (either to home page or add product page)
     */
    @PostMapping({"/add", "/edit/{id}"})
    public String saveProduct(@Valid @ModelAttribute("product") CUProductDTO cUProductDTO,
                              BindingResult result, Model model,
                              @RequestParam("files") MultipartFile[] files,
                              @PathVariable(value = "id", required = false) Long id) {
        // Check and create validation errors for the images field
        if (cUProductDTO.getIndexExistingImages() == null && files[0].isEmpty()) {
            result.rejectValue("validateImages", "validation.product.images.notEmpty",
                    getMessage("validation.product.images.notEmpty"));
        }

        // Check and throw validation errors if any
        if (result.hasErrors()) {
            createInfoCUPage(model, cUProductDTO, id);
            return PRODUCT_CU_VIEW;
        }

        try {
            // Handling when updating
            List<Integer> indexExistingImages = cUProductDTO.getIndexExistingImages();
            if (id != null && indexExistingImages != null) {
                List<String> productImages = productService.convertToDTO(productService.getProductById(id)).getImages();

                //check if there are any changes
                if (indexExistingImages.size() != productImages.size()) {
                    List<String> updatedExistingImages = new ArrayList<>();

                    // Update the existingImages field
                    for (int index : indexExistingImages) {
                        if (index < productImages.size()) {
                            updatedExistingImages.add(productImages.get(index));
                        }
                    }

                    // Save changes
                    cUProductDTO.setExistingImages(updatedExistingImages);
                } else {
                    // if there are not any changes
                    cUProductDTO.setExistingImages(productImages);
                }
            }

            // Save products to database
            productService.saveProduct(cUProductDTO, files);
        } catch (IOException e) {
            // Log to file
            logger.error("Error occurred while saving product images: ", e);
        }

        return (id != null) ? "redirect:/" : "redirect:/product/add";
    }

    /**
     * Controller method to handle deletion of a product by ID.
     *
     * @param id The ID of the product to delete
     * @return A redirection to the home page after deletion
     */
    @GetMapping("/delete/{id}")
    public String deleteProduct(@PathVariable("id") @NotNull Long id) {
        try {
            productService.deleteProductById(id);
        } catch (Exception e) {
            // Log to file
            logger.error(e);
        }

        return "redirect:/";
    }

    @GetMapping("/detail/{id}")
    public String detailProduct(@PathVariable("id") Long id,Model model){
        model.addAttribute("product", productService.convertToDTO(productService.getProductById(id)));
        return PRODUCT_VIEW_DETAIL;
    }
}
