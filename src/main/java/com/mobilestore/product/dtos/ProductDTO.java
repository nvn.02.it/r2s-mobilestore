package com.mobilestore.product.dtos;

import com.mobilestore.category.entities.Category;
import com.mobilestore.manufacturer.entities.Manufacturer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {
    private Long id;

    private String name;

    private Double price;

    private Integer quantityInStock;

    private String description;

    private Manufacturer manufacturer;

    private Category category;

    private String productCondition;

    private List<String> images;
}