package com.mobilestore.product.dtos;

import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CUProductDTO {
    private Long id;

    @NotEmpty(message = "{validation.product.name.notEmpty}")
    private String name;

    @NotNull(message = "{validation.product.price.notNull}")
    @PositiveOrZero(message = "{validation.product.price.positiveOrZero}")
    @Digits(integer = 10, fraction = 2, message = "{validation.product.price.digits}")
    private Double price;

    @NotNull(message = "{validation.product.quantityInStock.notNull}")
    @PositiveOrZero(message = "{validation.product.quantityInStock.positiveOrZero}")
    private Integer quantityInStock;

    @NotEmpty(message = "{validation.product.description.notEmpty}")
    private String description;

    @NotNull(message = "{validation.product.manufacturer.notNull}")
    private Long manufacturer;

    @NotNull(message = "{validation.product.category.notNull}")
    private Integer category;

    @NotEmpty(message = "{validation.product.condition.notEmpty}")
    private String productCondition;

    // Field for validate
    private String validateImages;

    // Field for update
    private List<String> existingImages;

    // Field for update
    private List<Integer> indexExistingImages;
}
