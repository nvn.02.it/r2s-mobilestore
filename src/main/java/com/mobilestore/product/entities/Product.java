package com.mobilestore.product.entities;

import com.mobilestore.category.entities.Category;
import com.mobilestore.manufacturer.entities.Manufacturer;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Double price;

    private Integer quantityInStock;

    @Lob // Sử dụng kiểu dữ liệu MEDIUMTEXT cho cột 'description'
    @Column(columnDefinition = "MEDIUMTEXT")
    private String description;

    @ManyToOne
    @JoinColumn(name = "manufacturer_id")
    private Manufacturer manufacturer;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    private String productCondition;

    @Column(columnDefinition = "MEDIUMBLOB")
    private List<byte[]> images;
}