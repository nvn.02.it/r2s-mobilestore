package com.mobilestore.product.services;

import com.mobilestore.category.repositories.CategoryRepository;
import com.mobilestore.manufacturer.repositories.ManufacturerRepository;
import com.mobilestore.product.dtos.CUProductDTO;
import com.mobilestore.product.dtos.ProductDTO;
import com.mobilestore.product.entities.Product;
import com.mobilestore.product.repositories.ProductRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import java.io.IOException;

/**
 * Service class for handling product-related operations.
 */
@Service
@AllArgsConstructor
public class ProductService {

    private static final int DEFAULT_PAGE_SIZE = 10;

    final ProductRepository productRepository;

    final ModelMapper modelMapper;

    private final ManufacturerRepository manufacturerRepository;

    private final CategoryRepository categoryRepository;

    private final MessageSource messageSource;

    /**
     * Saves a new product with associated images.
     *
     * @param cUProductDTO The DTO containing information about the new product.
     * @param files            An array of MultipartFiles representing images associated with the product.
     * @throws IOException if an I/O error occurs while reading the image files.
     */
    public void saveProduct(CUProductDTO cUProductDTO, MultipartFile[] files) throws IOException {
        Product product = modelMapper.map(cUProductDTO, Product.class);

        product.setManufacturer(manufacturerRepository.findFirstById(cUProductDTO.getManufacturer()));
        product.setCategory(categoryRepository.findFirstById(cUProductDTO.getCategory()));

        List<byte[]> images = new ArrayList<>();

        // Handle existing images
        if (cUProductDTO.getExistingImages() != null) {
            for (String base64Image : cUProductDTO.getExistingImages()) {
                images.add(Base64.getDecoder().decode(base64Image));
            }
        }

        // Handle new images
        if (files != null) {
            for (MultipartFile file : files) {
                // Check the file is not empty and has content
                if (file != null && !file.isEmpty()) {
                    images.add(file.getBytes());
                }
            }
        }

        product.setImages(images);

        productRepository.save(product);
    }

    /**
     * Retrieves a paginated list of all products and converts them to ProductDTO objects.
     *
     * @param page the page number to retrieve (1-based index).
     * @return a {@link Page} of {@link ProductDTO} containing the products for the requested page.
     */
    public Page<ProductDTO> getAllProduct(Integer page) {
        Pageable pageable = PageRequest.of(page - 1, DEFAULT_PAGE_SIZE);
        Page<Product> productPage = productRepository.findAll(pageable);

        List<ProductDTO> productDTOs = productPage.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());

        return new PageImpl<>(productDTOs, pageable, productPage.getTotalElements());
    }

    /**
     * Retrieves a product by its unique identifier and converts it to a ProductDTO.
     *
     * @param id the unique identifier of the product to be retrieved
     * @return a ProductDTO representing the product with the given ID
     */
    public Product getProductById(Long id){
        Product product = productRepository.findFirstById(id);

        if (product == null)
            throw new IllegalArgumentException(messageSource.getMessage("product.not-found", null, null));

        return product;
    }

    /**
     * Converts a Product entity to a ProductDTO object.
     *
     * @param product The Product entity to be converted.
     * @return A ProductDTO object representing the converted product.
     */
    public ProductDTO convertToDTO(Product product) {
        ProductDTO productDTO = modelMapper.map(product, ProductDTO.class);

        List<String> base64Images = product.getImages().stream()
                .map(image -> Base64.getEncoder().encodeToString(image))
                .collect(Collectors.toList());

        productDTO.setImages(base64Images);
        return productDTO;
    }

    /**
     * Delete a product by its ID.
     *
     * @param id The ID of the product to delete
     * @throws IllegalArgumentException if no product with the specified ID exists
     */
    public void deleteProductById(Long id) {
        productRepository.delete(getProductById(id));
    }

    /**
     * Retrieves a list of products based on the provided list of IDs.
     *
     * @param listId A list of IDs of the products to retrieve.
     * @return A list of products corresponding to the provided IDs.
     */
    public List<Product> getAllById(List<Long> listId){
        return productRepository.findAllById(listId);
    }
}
