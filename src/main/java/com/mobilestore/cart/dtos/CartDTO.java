package com.mobilestore.cart.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CartDTO {
    private Long id;
    private String name;
    private Double price;
    private Integer quantity;
}
