package com.mobilestore.cart.controllers;

import com.mobilestore.cart.dtos.CartDTO;
import com.mobilestore.invoice.services.InvoiceService;
import com.mobilestore.product.entities.Product;
import com.mobilestore.product.services.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cart/api")
@AllArgsConstructor
public class CartRestController {

    private  final InvoiceService invoiceService;
    private final ProductService productService;
    private final MessageSource messageSource;

    @PostMapping("/checkout")
    public ResponseEntity<String> checkoutCart(@RequestBody List<CartDTO> cartDTOList){
        List<Long> listIdProduct = cartDTOList.stream()
                .map(CartDTO::getId).collect(Collectors.toList());

        List<Product> products = productService.getAllById(listIdProduct);

        Map<Long, Product> productMap = products.stream()
                .collect(Collectors.toMap(Product::getId, Function.identity()));

        for (CartDTO cartDTO : cartDTOList) {
            Product product = productMap.get(cartDTO.getId());
            if (product == null || product.getQuantityInStock() < cartDTO.getQuantity()) {
                return ResponseEntity.badRequest().body(messageSource
                        .getMessage("cart.order.error",null,null));
            }
        }
        invoiceService.saveInvoice(cartDTOList,productMap);
        return ResponseEntity.ok(messageSource.getMessage("cart.order.success",null,null));
    }


}
