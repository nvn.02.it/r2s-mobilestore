package com.mobilestore.cart.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cart")
public class CartController {
    private final String VIEW_CART = "cart";

    @GetMapping("/view")
    public String showCart(){
        return VIEW_CART;
    }
}
