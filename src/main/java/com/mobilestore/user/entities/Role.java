package com.mobilestore.user.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

/**
 * Represents a role entity in the application.
 */
@Getter
@Setter
@Entity
@Table(name = "roles")
public class Role {
    @Id
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "role")
    private List<UserInfo> users;

    public Role(String name) {
        this.name = name;
    }

    public Role(Integer id,String name) {
        this.id=id;
        this.name = name;
    }

    public  Role (){

    }
}