package com.mobilestore.user.controllers;

import com.mobilestore.jwt.services.JwtService;
import com.mobilestore.user.dtos.UserDTO;
import com.mobilestore.user.entities.UserInfo;

import com.mobilestore.user.services.UserInfoService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

/**
 * Controller class for handling user-related operations.
 */
@Controller
@RequestMapping("/admin")
@AllArgsConstructor
public class UserController {


    private final JwtService jwtService;


    private final MessageSource messageSource;


    private final UserInfoService userInfoService;


    private final AuthenticationManager authenticationManager;

    /**
     * Displays the login form.
     *
     * @return The name of the login view template.
     */
    @GetMapping("/login")
    public String showLogin() {
        return "login";
    }

    /**
     * Authenticates the user and generates a JWT token.
     *
     * @param username The username of the user.
     * @param password The password of the user.
     * @param response The HTTP servlet response object.
     * @return The redirection URL after successful authentication.
     * @throws UsernameNotFoundException if the user with the given username is not found.
     */
    @PostMapping("/generateToken")
    public String authenticateAndGetToken(@RequestParam("username") String username,
                                          @RequestParam("password") String password,
                                          HttpServletResponse response) {
        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (UsernameNotFoundException ex) {
            throw new UsernameNotFoundException(
                    messageSource.getMessage("validation.user.error", null, null));
        }

        if (authentication.isAuthenticated()) {
            String token = jwtService.generateToken(username);
            Cookie cookie = new Cookie("token", token);
            cookie.setMaxAge(Integer.MAX_VALUE);
            cookie.setPath("/");
            response.addCookie(cookie);
            System.out.println("token: " + token);
            return "redirect:/category/";
        } else {
            throw new UsernameNotFoundException(
                    messageSource.getMessage("validation.user.error", null, null));
        }
    }

    /**
     * Displays the user registration form.
     *
     * @param model The Model object to add attributes to.
     * @return The name of the registration view template.
     */
    @GetMapping("register")
    public String showRegistrationForm(Model model) {
        UserDTO user = new UserDTO();
        model.addAttribute("user", user);
        return "register";
    }

    /**
     * Handles the submission of the user registration form.
     *
     * @param user   The UserDto object containing user information.
     * @param result The BindingResult object for validation result.
     * @param model  The Model object to add attributes to.
     * @return The redirection URL after successful registration.
     */
    @PostMapping("/register/save")
    public String registration(@Valid @ModelAttribute("user") UserDTO user,
                               BindingResult result,
                               Model model) {
        UserInfo existing = userInfoService.findByName(user.getName());
        if (existing != null) {
            result.rejectValue("name", null,
                    messageSource.getMessage("validation.user.exits", null, null));
        }
        if (result.hasErrors()) {
            model.addAttribute("user", user);
            return "register";
        }
        userInfoService.saveUser(user);
        return "redirect:/user/register?success";
    }
}
