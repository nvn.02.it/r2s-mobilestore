USE mobilestore;

-- Thêm role nếu chưa có
INSERT INTO roles (id, name) VALUES (1, 'ROLE_ADMIN') ON DUPLICATE KEY UPDATE name = VALUES(name);
INSERT INTO roles (id, name) VALUES (2, 'ROLE_MEMBER') ON DUPLICATE KEY UPDATE name = VALUES(name);

-- Thêm người dùng admin nếu chưa có
-- password là Abc@12345 đã mã hoá
INSERT INTO user_info (id,name, password, role_id)
VALUES (1,'admin', '$2a$10$Xp8Vg6ndwE2lWIOccxgJUeFmaFHULGDRNCbyLUxOjR1SWKRhC0nZm', 1)
ON DUPLICATE KEY UPDATE name = VALUES(name), password = VALUES(password), role_id = VALUES(role_id);

-- Khởi tạo dữ liệu cho bảng Manufacturer
INSERT INTO Manufacturer (id,name) VALUES
    (1, 'Apple'),
    (2, 'Samsung')
ON DUPLICATE KEY UPDATE name = VALUES(name);

-- Khởi tạo dữ liệu cho bảng Category
INSERT INTO Category (id,name) VALUES
    (1, 'Smart phone'),
    (2, 'Gaming phone')
ON DUPLICATE KEY UPDATE name = VALUES(name);

