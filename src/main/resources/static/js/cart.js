function addToCart(product) {
    let cart = JSON.parse(localStorage.getItem('cart')) || [];

    let existingProduct = cart.find(item => item.id === product.id);

    if (existingProduct) {
        existingProduct.quantity += 1;
    } else {
        cart.push(product);
    }
    localStorage.setItem('cart', JSON.stringify(cart));
}


function clearCart() {
    localStorage.removeItem('cart');
}

function displayCart() {
    let cart = JSON.parse(localStorage.getItem('cart')) || [];
    let cartContainer = $('#cart-container');
    cartContainer.empty();
    if (cart.length === 0) {
        cartContainer.append('<tr><td colspan="5">Giỏ hàng của bạn đang trống.</td></tr>');
        $('#totalCart').text('0');
    } else {
        let total = 0;
        cart.forEach(item => {
            let itemTotal = item.price * item.quantity;
            total += itemTotal;
            let row = `
                <tr>
                    <td>${item.name}</td>
                    <td>${item.quantity}</td>
                    <td>${item.price}</td>
                    <td>${itemTotal}</td>
                    <td>
                        <button class="btn btn-danger remove-item" data-id="${item.id}">
                            <i class="fa-sharp fa-solid fa-xmark" style="color: #ffffff;"></i> Remove
                        </button>
                    </td>
                </tr>
            `;
            cartContainer.append(row);
        });
        $('#totalCart').text(total);
        $('.remove-item').click(function () {
            let id = $(this).data("id");
            console.log(id);
            cart = cart.filter(item => item.id != id);
            localStorage.setItem('cart', JSON.stringify(cart)); 
            displayCart(); 
        });
        

       
    }
}

 function showToast(name) {
            var toastEl = document.getElementById('myToast');
            $('.toast-body').empty()
            $('.toast-body').append('<p>Đã thêm '+name+' vào giỏ hàng</p>')
            var toast = new bootstrap.Toast(toastEl);
            toast.show();
        }
        function showToastFail(name) {
            var toastEl = document.getElementById('myToast');
            $('.toast-body').empty()
            $('.toast-body').append('<p>Sản phẩm '+name+' đã hết hàng</p>')
            var toast = new bootstrap.Toast(toastEl);
            toast.show();
        }
