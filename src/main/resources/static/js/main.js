$(document).ready(function () {
    $("#btnsubmit").click(function (e) {
        e.preventDefault();
        var isValid = true;
    
       
        $('.form input').each(function () {
            if ($(this).val() === '') {
                isValid = false;
                $(this).siblings('p.text-danger').text('Không được để trống').show();
            } else {
                $(this).siblings('p.text-danger').hide();
            }
        });

        var emailInput = $('#email');
        if (emailInput.length > 0 && emailInput.val().trim() != '') {
            var emailValue = emailInput.val();
            var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            if (!emailRegex.test(emailValue)) {
                isValid = false;
                emailInput.siblings('p.text-danger').text('Định dạng email không hợp lệ').show();
            } else {
                emailInput.siblings('p.text-danger').hide();
            }
        }

    
        if (isValid) {
            $('.form').submit();
        }
    });

    $("#showPassWord").click(function (e) { 
            const passwordInput = $("#password");
            const toggleIcon = $(".toggle-icon");
    
            if (passwordInput.attr("type") === "password") {
                passwordInput.attr("type", "text");
                toggleIcon.html('<i class="bi bi-eye-slash"></i>'); // Icon mắt mở
            } else {
                passwordInput.attr("type", "password");
                toggleIcon.html('<i class="bi bi-eye"></i>'); // Icon mắt đóng
            }
        
    });
   
    // ADD PRODUCT FORM
    $('#add-product-form .form-control').focus(function() {
        $(this).select();
    });

    $('#addProductBtn').keydown(function(event) {
        if (event.key === 'Tab') {
            event.preventDefault();
            $('#productName').focus();
        }
    });


  

});

